$j(".toggle-btn-accordion .ui-accordion-header").each(function() {
    $j(this).parent().append($j(this));
});

$j(".toggle-btn-accordion .ui-accordion-header").click(function() {
    var obj = $j(this);
    obj.siblings(".accordion_content").slideToggle();
    obj.text(obj.text() == 'READ MORE' ? 'READ LESS' : 'READ MORE');
});

/* Custom Event - Single Event */
$j(".event-content .end_hour ~  .custom_link a").attr('href', '#gmap_id');

$j('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $jtarget = $j(target);

        $j('html, body').stop().animate({
            'scrollTop': $jtarget.offset().top - 100
        }, 800, 'swing', function () {
            window.location.hash = target;
        });
    });

/* Fix Click Event Link on Accordion */
jQuery(".accordion_content a").click(function(){window.open(jQuery(this).attr('href'));});

/* Custom Width For Mobile Menu */
jQuery("header nav.mobile_menu").width(jQuery(window).width() - 90);
  $j(window).resize(function() {
jQuery("header nav.mobile_menu").width(jQuery(window).width() - 90);
});


/* Remove Text Sign Up When Sign Up Done */
if(jQuery(".swpm-registration-success-msg").length) {
              jQuery(".form-title").remove()
}

/* Display Mobile Menu */
    jQuery(".mobile_menu .menu-bar-btn i").click(function(){
        jQuery(".mobile_menu ul[id^=menu-mobile-menu]").slideToggle();
    });

 /* Focus for DatePicker and icon */
jQuery(".from-date-input").click(function(){
    jQuery(".hasDatepicker[name='from']").focus();
});

jQuery(".to-date-input").click(function(){
        jQuery(".hasDatepicker[name='to']").focus();
});